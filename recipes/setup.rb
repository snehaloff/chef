package 'ntp' do
  action :install
end

package 'dstat'

package 'tree' do
  action :install
end

package 'git' do
  action :install
end

file '/etc/motd' do
  content 'This property is of Junglr.inc'
  owner 'root'
  group 'root'
end 
  
service 'ntpd' do
  action [ :enable, :start ]
end
